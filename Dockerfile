FROM node:18.17.0 AS build-stage

WORKDIR /app

COPY . .

RUN npm install -g npm@10.2.0

RUN npm ci

RUN npm run build

FROM nginx:1.19.0-alpine as production-stage

COPY --from=build-stage /app/dist /usr/share/nginx/html

# COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
