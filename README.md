# Radio Vue App

This project was created for a test assignment to a company COXIT ❤️‍🔥

## Was implemented 

1. Display all the tracks in the json file, starting with the “playing” item at the top of the
   page and then the “history” items in a list below.
2. The “playing” item should show a progress bar indicating the play time of the track.
3. The “history” items should be shown in reverse chronological order. (Most recent at
   the top, oldest at the bottom)
4. For each item display the following:
   - Title
   - Artist
   - Album
   - ImageUrl (as an image)
   - Duration
   - StartTime (in the local time of the browser)
   

## Stack 💪

- Vue 3
- TypeScript
- Axios
- Vite
- Vitest
- Docker
- Gitlab CI/CD
- SCSS


## Features done

- Error handling (for network issues and when is no picture, or it is not on the link - it is replaced by a placeholder)
- Unit tests have been written
- Docker file created
- Configured CI/CD with node_modules caching. Stages: testing, lint, build and deployment to GitLab Container Registry
- Adaptive and responsive UI
- It is easy to embed in a large project, because the component to display the radio sheet - is in the form of a module in the widgets folder. I did this looking back at my previous work, where we had just such a folder and my solution would fit in there perfectly. Inside the module the responsibilities are divided into different files, which is a good thing in modern development
- It turned out to be a cool design, I think
- Resolved a problem with CORS for local operation

I'm very much looking forward to feedback on the work done, I'm always ready for criticism, which will encourage me to develop! 😉