import { TrackService } from "./track.service";
import { assert, test, describe, beforeEach } from "vitest";

// Testing TrackService
describe("TrackService", () => {
  let trackService: TrackService;

  beforeEach(() => {
    trackService = new TrackService();
  });

  test("should retrieve the playing track and history tracks", async () => {
    const {
      playingTrackResponse,
      historyTracksResponse,
      errorMessageResponse,
    } = await trackService.getTracks();

    // Assertions
    assert.isNotNull(playingTrackResponse);
    assert.isArray(
      historyTracksResponse,
    );
    assert.equal(
        historyTracksResponse.length,
        24
    );
    assert.equal(errorMessageResponse, null);
  });
});
