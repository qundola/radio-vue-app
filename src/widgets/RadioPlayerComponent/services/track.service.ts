import { ITrack } from "../types/track.ts";
import axios, {AxiosResponse} from "axios";

interface TrackResponse {
  nowplaying: ITrack[];
}

interface TrackResult {
  playingTrackResponse: playingTrackType;
  historyTracksResponse: historyTracksType;
  errorMessageResponse: errorMessageType;
}

type playingTrackType = ITrack | null;
type historyTracksType = ITrack[];
type errorMessageType = String | null;

export class TrackService {
  public async getTracks(): Promise<TrackResult> {
    let playingTrackResponse: playingTrackType = null;
    let historyTracksResponse: historyTracksType = [];
    let errorMessageResponse: errorMessageType = null;

    try {
      let response: AxiosResponse<TrackResponse> | null
      if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test'){
        response = await axios('https://cors-anywhere.herokuapp.com/https://onair.radioapi.io/thisisgo/go/onair.json', {
          method: 'get',
          headers: {
            'x-requested-with': 'XMLHttpRequest',
            'origin': 'https://onair.radioapi.io/'
          }
        })
      } else {
        response = await axios('https://onair.radioapi.io/thisisgo/go/onair.json', {
          method: 'get'
        })
      }

      if(response){
        const data = response.data as TrackResponse
        playingTrackResponse = data.nowplaying[0]
        historyTracksResponse = data.nowplaying.slice(1)
      }
    } catch (error) {
      errorMessageResponse = "Error fetching data from server!";
    }

    return {
      playingTrackResponse,
      historyTracksResponse,
      errorMessageResponse,
    };
  }
}
